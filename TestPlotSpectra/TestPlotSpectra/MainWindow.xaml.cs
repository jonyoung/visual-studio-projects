﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using Microsoft.VisualBasic.FileIO;

namespace TestPlotSpectra
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string[] xcoords;
        public SeriesCollection ycoords;
        public Func<double, string> Formatter { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            string spectraFile = @"D:\work\pentip\tmp\Spot 1.csv";
            PlotData spectraData = ReadSpectra(spectraFile);
            xcoords = spectraData.X.Select(x => Convert.ToString(x)).ToArray();
            ycoords = spectraData.Y;
            Formatter = value => value.ToString("N");
            DataContext = this;
        }

        private PlotData ReadSpectra(string filename)
        {
            PlotData plotData = new PlotData();
            plotData.Y = new SeriesCollection
            {
                new ColumnSeries
                {
                    Values = new ChartValues<float>()
                }
            };

            int numHeaderLines = 8;
            using (TextFieldParser csvParser = new TextFieldParser(filename))
            {
                csvParser.TextFieldType = FieldType.Delimited;
                csvParser.SetDelimiters(",");
                for (int i = 0; i < numHeaderLines; i++)
                {
                    csvParser.ReadFields();
                }
                while (!csvParser.EndOfData)
                {
                    string[] fields = csvParser.ReadFields();
                    plotData.X.Add(Convert.ToSingle(fields[0]));
                    plotData.Y[0].Values.Add(Convert.ToSingle(fields[1]));
                }
            }
            return plotData;
        }
    }

    public struct PlotData
    {
        public List<float> X;
        public SeriesCollection Y;
    }
}
